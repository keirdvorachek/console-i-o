//Keir Dvorachek
// Console I/O

#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
	// Setting up variables for grabbing dynamic user input and setting the parameter for the loop to run.
	int intVariable;
	int intCounter = 0;
	cout << "Please enter an integer between the numbers 1 and 5. \n";
	cin >> intVariable;
	// If statement narrowing the parameters of what the user is allowed to enter.
	if (intVariable >= 1 && intVariable <= 5)
	{
		// Loop that runs for the amount of cycles that the user specifies.
		while (intCounter != intVariable)
		{
			cout << "\"Those who dare to fail miserably can achieve greatly.\" \t-John F. Kennedy\n";
			intCounter++;
		}
	}
	// Error message for the user if they choose a number outside the parameters.
	else
	{
		cout << "You failed miserably by entering an invalid number.";
	}

	_getch();
	return 0;
}
